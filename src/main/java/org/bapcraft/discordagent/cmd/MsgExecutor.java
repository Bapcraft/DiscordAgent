package org.bapcraft.discordagent.cmd;

import java.util.Optional;

import org.bapcraft.discordagent.api.DiscordAgentService;
import org.bapcraft.discordagent.storage.AgentStorage;
import org.bapcraft.discordagent.storage.UserProfile;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;

public class MsgExecutor implements CommandExecutor {

	private AgentStorage storage;
	private DiscordAgentService serv;

	public MsgExecutor(AgentStorage storage, DiscordAgentService serv) {
		this.storage = storage;
		this.serv = serv;
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

		User user = (User) args.getOne("user").get();
		String msg = (String) args.getOne("msg").get();
		StringBuilder sb = new StringBuilder();

		// Set up the header of the message.
		sb.append("**From ");
		sb.append(src.getName());

		// Make sure the destination user has a profile registered.
		UserProfile destProf = this.storage.getDiscordUser(user.getUniqueId());
		if (destProf == null) {
			src.sendMessage(Text.of("The destination user has not linked a Discord account yet."));
			return CommandResult.empty();
		}

		// Make sure the destination user hasn't ignored the sender.
		if (src instanceof Player) {

			Player sender = (Player) src;
			if (destProf.isIgnoring(sender.getUniqueId())) {
				return CommandResult.empty();
			}

		}

		// Now check to see if the player has registered with us, and include their mention in the message.
		if (src instanceof Player) {
			Player p = (Player) src;
			Optional<String> mentionOpt = this.serv.getMentionOf(p.getUniqueId());
			if (mentionOpt.isPresent()) {
				sb.append(" (" + mentionOpt.get() + ")");
			}
		}

		// Now close off the header and add the message.
		sb.append(":** ");
		sb.append(msg);

		// Actually send it.
		this.serv.sendMessage(user.getUniqueId(), sb.toString());

		return CommandResult.success();

	}

}
