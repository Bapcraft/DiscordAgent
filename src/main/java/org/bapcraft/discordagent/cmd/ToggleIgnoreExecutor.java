package org.bapcraft.discordagent.cmd;

import java.util.UUID;

import org.bapcraft.discordagent.storage.AgentStorage;
import org.bapcraft.discordagent.storage.UserProfile;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;

public class ToggleIgnoreExecutor implements CommandExecutor {

	private AgentStorage storage;

	public ToggleIgnoreExecutor(AgentStorage storage) {
		this.storage = storage;
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

		if (!(src instanceof Player)) {
			src.sendMessage(Text.of("You must be a player to execute this command!"));
			return CommandResult.empty();
		}

		Player p = (Player) src;
		UUID playerId = p.getUniqueId();

		User u = (User) args.getOne("user").get();
		UUID ignoreId = u.getUniqueId();

		// Load the player's discord profile.
		UserProfile prof = this.storage.getDiscordUser(playerId);
		if (prof == null) {
			src.sendMessage(Text.of("No profile found, have you linked your account yet?"));
			return CommandResult.empty();
		}

		// This is where we actually do the work.
		if (!prof.isIgnoring(ignoreId)) {
			prof.addIgnore(ignoreId);
			src.sendMessage(Text.of("Now ignoring " + u.getName()));
		} else {
			prof.removeIgnore(ignoreId);
			src.sendMessage(Text.of("No longer ignoring " + u.getName()));
		}

		// Save it.
		this.storage.setDiscordUser(playerId, prof);

		return CommandResult.success();

	}

}
