package org.bapcraft.discordagent.storage;

import java.util.HashSet;
import java.util.UUID;

public class UserProfile {

	public long snowflakeId;
	public String discordUsername;
	public String discordDiscrim;
	public String discordMention;

	public HashSet<UUID> ignoredUsers = new HashSet<>();

	public UserProfile(long snowflake, String username, String discrim, String mention) {
		this.snowflakeId = snowflake;
		this.discordUsername = username;
		this.discordDiscrim = discrim;
		this.discordMention = mention;
	}
	
	/**
	 * Sometimes fields will be null when importing data from older versions of the
	 * plugin.
	 */
	private void sanityCheckAndFix() {
		if (this.ignoredUsers == null) {
			this.ignoredUsers = new HashSet<>();
		}
	}

	public void addIgnore(UUID uuid) {
		this.sanityCheckAndFix();
		this.ignoredUsers.add(uuid);
	}

	public boolean removeIgnore(UUID uuid) {
		this.sanityCheckAndFix();
		return this.ignoredUsers.remove(uuid);
	}

	public boolean isIgnoring(UUID uuid) {
		this.sanityCheckAndFix();
		return this.ignoredUsers.contains(uuid);
	}

}
